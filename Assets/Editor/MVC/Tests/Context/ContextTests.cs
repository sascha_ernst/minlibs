using System.Reflection;
using NUnit.Framework;

namespace MinLibs.MVC
{
	public class ContextTests
	{
		Context context;

		[SetUp]
		public void Setup ()
		{
			context = new Context
			{
				ContextFlags = ContextFlags.Exception | ContextFlags.AutoResolve,
				BindingFlags = BindingFlags.SetProperty | BindingFlags.SetField
			};
		}

		[Test]
		public void IsSetupCorrect ()
		{
			Assert.IsTrue(context.Has<IContext>());
		}

		[Test]
		public void RegistersClassByTemplate ()
		{
			context.RegisterClass<TestClass>();
			Assert.IsTrue(context.Has<TestClass>());
		}

		[Test]
		public void RegistersInterfaceByTemplate ()
		{
			context.RegisterClass<ITestClass, TestClass>();
			Assert.IsTrue(context.Has<ITestClass>());
			Assert.IsFalse(context.Has<TestClass>());

			var instance = context.Get<ITestClass>();
			Assert.NotNull(instance);
		}

		[Test]
		public void RegistersClassByType ()
		{
			var type = typeof(TestClass);
			context.RegisterType(type);
			Assert.IsTrue(context.Has(type));
		}

		[Test]
		public void RegistersInterfaceByType ()
		{
			var interfaceType = typeof(ITestClass);
			var classType = typeof(TestClass);
			context.RegisterType(interfaceType, classType);
			Assert.IsTrue(context.Has(interfaceType));
			Assert.IsFalse(context.Has(classType));
		}

		[Test]
		public void RegistersInstanceAlwaysCached ()
		{
			var instance = new TestClass();
			context.RegisterInstance(instance);
			Assert.IsTrue(context.Has<TestClass>());

			var retrieved = context.Get<TestClass>();
			Assert.AreSame(instance, retrieved);
			Assert.NotNull(instance.fieldInjection);
		}

		[Test]
		public void CachesInstances ()
		{
			var inst1 = context.Get<TestClass>();
			var inst2 = context.Get<TestClass>();
			Assert.AreSame(inst1, inst2);
		}

		[Test]
		public void PreventsCaching ()
		{
			context.RegisterClass<TestClass>(RegisterFlags.NoCache);
			var inst1 = context.Get<TestClass>();
			var inst2 = context.Get<TestClass>();
			var inst3 = context.Get<TestClass>();

			Assert.AreNotSame(inst1, inst2);
			Assert.AreNotSame(inst2, inst3);
		}

		[Test]
		public void UnregisterByClass ()
		{
			context.RegisterClass<ITestClass, TestClass>();
			Assert.IsTrue(context.Has<ITestClass>());

			context.Unregister<ITestClass>();
			Assert.IsFalse(context.Has<ITestClass>());
		}

		[Test]
		public void UnregisterByType ()
		{
			var type = typeof(TestClass);
			context.RegisterType(type);
			Assert.IsTrue(context.Has(type));

			context.Unregister(type);
			Assert.IsFalse(context.Has(type));
		}

		[Test]
		public void ThrowsExceptionWhenTypeNotRegistered ()
		{
			context.ContextFlags &= ~ContextFlags.AutoResolve;
			Assert.Throws<NotRegisteredException>(() => context.Get<ITestClass>());
		}

		[Test]
		public void InjectsIntoFieldOfRegisteredInstance ()
		{
			var instance = context.Get<TestClass>();
			Assert.NotNull(instance.fieldInjection);
		}

		[Test]
		public void InjectsIntoPropertyOfRegisteredInstance ()
		{
			var instance = context.Get<TestClass>();
			Assert.NotNull(instance.propertyInjection);
		}

		[Test]
		public void WarnsAboutHidingInjectionInParentClass () {
			Assert.Throws<AlreadyInjectedException>(() => context.Get<InheritedTestClassWithDuplicateInjection>());
		}

		[Test]
		public void ThrowsExceptionIfInjectionIsNotRegistered ()
		{
			context.ContextFlags &= ~ContextFlags.AutoResolve;
			Assert.Throws<NotRegisteredException>(() => context.Get<TextClassInjectingInterface>());
		}

		[Test]
		public void ThrowsCannotRegisterInterfaceAsValueException ()
		{
			Assert.Throws<CannotRegisterInterfaceAsValueException>(() => context.RegisterType(typeof(ITestClass)));
		}

		[Test]
		public void InjectsIntoNonRegisterdInstance ()
		{
			var instance = new TestClass();
			context.Inject(instance);
			Assert.NotNull(instance.fieldInjection);
		}

		[Test]
		public void InjectsInCircle ()
		{
			var instance1 = context.Get<CircularClass1>();
			Assert.NotNull(instance1);
		}

		[Test]
		public void InjectInstancesInCircle ()
		{
			var instance1 = new CircularClass1();
			var instance2 = new CircularClass2();

			context.RegisterInstance(instance1);
			context.RegisterInstance(instance2);
			context.Get<CircularClass1>();
		}

		[Test]
		public void InjectsWithHandler ()
		{
			context.RegisterHandler<ITestClassWithHost>(host => new TestClassWithHost(host));
			var testInstance = context.Get<ITestClassWithHost>(this);
			Assert.AreSame(this, testInstance.Host);
		}

		[Test]
		public void CallsPostInjection ()
		{
			var instance = context.Get<TestClass>();
			Assert.IsTrue(instance.isPostInjected);
		}

		[Test]
		public void PreventsPostInjection ()
		{
			context.InjectorFlags |= InjectorFlags.PreventPostInjection;
			var instance = context.Get<TestClass>();
			Assert.IsFalse(instance.isPostInjected);
		}

		[Test]
		public void CleansupContext ()
		{
			context.RegisterClass<TestClass>();
			var instance = context.Get<TestClass>();
			Assert.IsFalse(instance.isCleanedUp);

			context.CleanUp();
			Assert.IsTrue(instance.isCleanedUp);
		}
	}
}
