﻿using MinLibs.MVC.TestUtils;
using NUnit.Framework;
using NSubstitute;
using MinLibs.Signals;

namespace MinLibs.MVC
{
	public class MediatorTests : MediatorTests<TestMediator, IMediated>
	{
		protected override void SetUpSubject ()
		{
			base.SetUpSubject();

			subject.dispatcher = new Signal();
		}

		[Test]
		public void RegistersListeners ()
		{
			subject.dispatcher.Dispatch();
			view.Received(1).Remove();
		}

		[Test]
		public void should_cleanup_on_remove ()
		{
			verifiers.Add(subject.dispatcher);
			view.OnRemove.Dispatch();
		}
	}
}