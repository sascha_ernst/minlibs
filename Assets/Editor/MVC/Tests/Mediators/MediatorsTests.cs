﻿using NUnit.Framework;
using NSubstitute;

namespace MinLibs.MVC
{
	public class MediatorsTests
	{
		Mediators mediators;

		[SetUp]
		public void Setup ()
		{
			mediators = new Mediators();
			mediators.context = Substitute.For<IContext>();
		}

		[Test]
		public void MediatesView ()
		{
			var mediator = Substitute.For<TestMediator>();
			var view = Substitute.For<IMediated>();
			mediators.context.Get<IMediator>(typeof(TestMediator)).Returns(mediator);
			mediators.Map<IMediated, TestMediator>();
			mediators.Mediate(view);
			mediator.Received(1).Init(view);
			view.Received(1).HandleMediation();
		}
	}
}
