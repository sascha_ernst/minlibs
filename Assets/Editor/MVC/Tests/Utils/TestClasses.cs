﻿using MinLibs.Signals;

namespace MinLibs.MVC
{
	public interface ITestClass
	{
	}

	public class TestClass : ITestClass
	{
		public bool isPostInjected;
		public bool isCleanedUp;

		[Inject] public TestInjection fieldInjection;

		[Inject] public TestInjection propertyInjection { get; set; }

		[PostInjection]
		public void PostInjection ()
		{
			isPostInjected = true;
		}

		public TestInjection GetFieldInjection ()
		{
			return fieldInjection;
		}

		[Cleanup]
		public void Cleanup ()
		{
			isCleanedUp = true;
		}
	}

	public class TextClassInjectingInterface
	{
		[Inject] public ITestClass injection;
	}

	public class TestInjection
	{
	}

	public class CircularClass1
	{
		[Inject] public CircularClass2 circular;
	}

	public class CircularClass2
	{
		[Inject] public CircularClass1 circular;
	}

	public class TestMediator : Mediator<IMediated>
	{
		public Signal dispatcher;

		protected override void Register ()
		{
			base.Register();

			signals.Register(dispatcher, mediated.Remove);
		}

		protected override void OnRemove ()
		{
			base.OnRemove();

			dispatcher.Dispatch();
		}
	}

	public interface ITestClassWithHost
	{
		object Host { get; }
	}

	public class TestClassWithHost : ITestClassWithHost
	{
		public object Host { get; private set; }

		public TestClassWithHost (object host)
		{
			Host = host;
		}
	}

	public class InheritedTestClassWithDuplicateInjection : TestClass
	{
#pragma warning disable 108,114
		[Inject] public TestInjection fieldInjection;
#pragma warning restore 108,114
	}
}
