﻿using UnityEditor;

namespace MinLibs
{
	public static class MinLibsEditorScripts
	{
		[MenuItem("MinLibs/Create Package &%p")]
		public static void CreatePackage()
		{
			AssetDatabase.ExportPackage("Assets/MinLibs", "MinLibs.unitypackage", ExportPackageOptions.Recurse);
		}
	}
}