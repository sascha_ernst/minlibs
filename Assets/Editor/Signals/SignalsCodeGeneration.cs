﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;

namespace MinLibs.Signals
{
	public static class SignalsCodeGeneration
	{
		static readonly int[] ARITIES_TO_GENERATE = { 0, 2, 3 };

		const string SRC_PATH = "Assets/MinLibs/Signals/";
		const string EDITOR_PATH = "Assets/Editor/Signals/";
		const string GENERATED_SIGNALS_PATH_FORMAT = SRC_PATH + "Signals/Signal{0}.cs";
		const string GENERATED_EXTENSIONS_PATH_FORMAT = SRC_PATH + "Extensions/Signal{0}Extensions.cs";
		const string GENERATED_TESTS_PATH_FORMAT = EDITOR_PATH + "Tests/Signals/Signal{0}Tests.cs";
		const string GENERATED_VERIFIERS_PATH_FORMAT = SRC_PATH + "Editor/SignalVerifiers/Signal{0}Verifiers.cs";

		static readonly Dictionary<string, List<string>> REPLACEMENTS_IN_SOURCE = new Dictionary<string, List<string>> {
			{ "<T0>", new List<string> { "", "<T0, T1>", "<T0, T1, T2>" } },
			{ "(T0 t0)", new List<string> { "()", "(T0 t0, T1 t1)", "(T0 t0, T1 t1, T2 t2)" } },
			{ "(t0)", new List<string> { "()", "(t0, t1)", "(t0, t1, t2)" } },
			{ "<T0, bool>", new List<string> { "<bool>", "<T0, T1, bool>", "<T0, T1, T2, bool>" } },
		};

		static readonly Dictionary<string, List<string>> REPLACEMENTS_IN_TESTS = new Dictionary<string, List<string>> {
			{ "(V_0)", new List<string> { "()", "(V_0, V_1)", "(V_0, V_1, V_2)" } },
			{ "Signal1", new List<string> { "Signal0", "Signal2", "Signal3" } },
			{ "Signal<int>", new List<string> { "Signal", "Signal<int,int>", "Signal<int,int,int>" } },
			{ "(int t0)", new List<string> { "()", "(int t0, int t1)", "(int t0, int t1, int t2)" } },
			{ "(t0)", new List<string> { "()", "(t0, t1)", "(t0, t1, t2)" } },
		};

		[MenuItem("MinLibs/Run Signals Code Generator")]
		public static void Generate ()
		{
			foreach (var arity in ARITIES_TO_GENERATE) {
				// source files
				ReplaceAndWrite(GENERATED_SIGNALS_PATH_FORMAT, arity, REPLACEMENTS_IN_SOURCE);

				// extension files
				ReplaceAndWrite(GENERATED_EXTENSIONS_PATH_FORMAT, arity, REPLACEMENTS_IN_SOURCE);

				// test files
				ReplaceAndWrite(GENERATED_TESTS_PATH_FORMAT, arity, REPLACEMENTS_IN_TESTS);

				// verifier files
				ReplaceAndWrite(GENERATED_VERIFIERS_PATH_FORMAT, arity, REPLACEMENTS_IN_SOURCE);
			}

			AssetDatabase.Refresh();
		}

		static void ReplaceAndWrite (string path, int arity, Dictionary<string, List<string>> allReplacements)
		{
			var sourcePath = string.Format(path, 1);
			var text = File.ReadAllText(sourcePath);
			var replacements = PickReplacementsForArity(arity, allReplacements);

			foreach (var replacementKvp in replacements) {
				text = text.Replace(replacementKvp.Key, replacementKvp.Value);
			}

			var outPath = string.Format(path, arity);
			File.WriteAllText(outPath, text);
		}

		static Dictionary<string, string> PickReplacementsForArity (int arity, Dictionary<string, List<string>> allReplacements)
		{
			if (!ARITIES_TO_GENERATE.Contains(arity)) {
				throw new System.NotSupportedException("Arity not supported: " + arity);
			}

			var indexToPick = arity > 1 ? arity - 1 : 0;
			var result = new Dictionary<string, string>();

			foreach (var kvp in allReplacements) {
				result[kvp.Key] = kvp.Value[indexToPick];
			}

			return result;
		}
	}
}