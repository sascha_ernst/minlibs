﻿using System;
using NUnit.Framework;

namespace MinLibs.Signals
{
	[TestFixture]
	public class SignalVerifierTests
	{
		[TestCase(1, true)]
		[TestCase(2, false)]
		public void should_verify_dispatch (int expectedCalls, bool expectedSuccess)
		{
			const int ID = 3;
			var signal = new Signal<int>();
			var verifier = new SignalVerifier<int>(signal, expectedCalls, id => id == ID);
			
			signal.Dispatch(ID);

			var caughtException = false;
			
			try {
				verifier.Verify();
			}
			catch {
				caughtException = true;
				
				if (expectedSuccess) {
					throw;
				}
			}
			
			Assert.AreNotEqual(expectedSuccess, caughtException);
		}
	}
}