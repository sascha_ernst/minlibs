using NSubstitute;
using NUnit.Framework;

namespace MinLibs.Signals
{
	[TestFixture]
	public class Signal0Tests : SignalTestsBase
	{
		Signal signal;
		IListener listener;

		[SetUp]
		public void Setup ()
		{
			signal = new Signal();
			listener = Substitute.For<IListener>();
		}

		[Test]
		public void should_dispatch ()
		{
			signal.AddListener(listener.Method1);
			signal.Dispatch();
			listener.Received(1).Method1();
		}

		[Test]
		public void should_call_all_listeners ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.Dispatch();

			listener.Received(1).Method1();
			listener.Received(1).Method2();
		}

		[Test]
		public void should_add_listener_only_once ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method1);
			signal.Dispatch();

			listener.Received(1).Method1();
		}

		[Test]
		public void can_remove_during_execution ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(() => signal.RemoveListener(listener.Method2));
			signal.AddListener(listener.Method2);
			signal.Dispatch();

			listener.Received(1).Method1();
			listener.DidNotReceive().Method2();
		}

		[Test]
		public void can_remove_all_during_execution ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(() => { signal.RemoveAllListeners(); });
			signal.AddListener(listener.Method2);
			signal.Dispatch();

			listener.Received(1).Method1();
			listener.DidNotReceive().Method2();
		}

		[Test]
		public void can_remove_listener_in_dispatch ()
		{
			signal.AddListener(listener.Method2);
			signal.AddListener(TestOnceListener);
			signal.Dispatch();
			// once listener should be removed here
			signal.Dispatch();
			listener.Received(1).Method1();
			listener.Received(2).Method2();
		}

		private void TestOnceListener ()
		{
			signal.RemoveListener(TestOnceListener);
			listener.Method1();
		}

		[Test]
		public void should_have_listener_after_adding_one ()
		{
			signal.AddListener(listener.Method1);

			Assert.That(signal.HasListeners(), Is.True);

			signal.Dispatch();

			listener.Received(1).Method1();
		}

		[Test]
		public void should_have_listeners_after_adding_multiple ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);

			Assert.That(signal.HasListeners(), Is.True);

			signal.Dispatch();

			listener.Received(1).Method1();
			listener.Received(1).Method2();
		}

		[Test]
		public void should_have_no_listeners_after_removing_the_only_one ()
		{
			signal.AddListener(listener.Method1);
			signal.RemoveListener(listener.Method1);

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch();

			listener.DidNotReceive().Method1();
		}

		[Test]
		public void should_have_no_listeners_after_removing_the_last_one ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.RemoveListener(listener.Method1);
			signal.RemoveListener(listener.Method2);

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch();

			listener.DidNotReceive().Method1();
			listener.DidNotReceive().Method2();
		}

		[Test]
		public void should_add_and_remove_listener_with_manage ()
		{
			signal.ManageListener(listener.Method1, true);
			
			Assert.That(signal.HasListeners(), Is.True);

			signal.ManageListener(listener.Method1, false);
			
			Assert.That(signal.HasListeners(), Is.False);
		}

		[Test]
		public void should_have_no_listeners_after_removing_all ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.RemoveAllListeners();

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch();

			listener.DidNotReceive().Method1();
			listener.DidNotReceive().Method2();
		}

		public interface IListener
		{
			void Method1 ();
			void Method2 ();
		}
	}
}
