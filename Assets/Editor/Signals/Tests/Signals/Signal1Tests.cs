﻿using NSubstitute;
using NUnit.Framework;

namespace MinLibs.Signals
{
	[TestFixture]
	public class Signal1Tests : SignalTestsBase
	{
		Signal<int> signal;
		IListener listener;

		[SetUp]
		public void Setup ()
		{
			signal = new Signal<int>();
			listener = Substitute.For<IListener>();
		}

		[Test]
		public void should_dispatch ()
		{
			signal.AddListener(listener.Method1);
			signal.Dispatch(V_0);
			listener.Received(1).Method1(V_0);
		}

		[Test]
		public void should_call_all_listeners ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
			listener.Received(1).Method2(V_0);
		}

		[Test]
		public void should_add_listener_only_once ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method1);
			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
		}

		[Test]
		public void can_remove_during_execution ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener((int t0) => signal.RemoveListener(listener.Method2));
			signal.AddListener(listener.Method2);
			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
			listener.DidNotReceive().Method2(V_0);
		}

		[Test]
		public void can_remove_all_during_execution ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener((int t0) => { signal.RemoveAllListeners(); });
			signal.AddListener(listener.Method2);
			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
			listener.DidNotReceive().Method2(V_0);
		}

		[Test]
		public void can_remove_listener_in_dispatch ()
		{
			signal.AddListener(listener.Method2);
			signal.AddListener(TestOnceListener);
			signal.Dispatch(V_0);
			// once listener should be removed here
			signal.Dispatch(V_0);
			listener.Received(1).Method1(V_0);
			listener.Received(2).Method2(V_0);
		}

		private void TestOnceListener (int t0)
		{
			signal.RemoveListener(TestOnceListener);
			listener.Method1(t0);
		}

		[Test]
		public void should_have_listener_after_adding_one ()
		{
			signal.AddListener(listener.Method1);

			Assert.That(signal.HasListeners(), Is.True);

			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
		}

		[Test]
		public void should_have_listeners_after_adding_multiple ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);

			Assert.That(signal.HasListeners(), Is.True);

			signal.Dispatch(V_0);

			listener.Received(1).Method1(V_0);
			listener.Received(1).Method2(V_0);
		}

		[Test]
		public void should_have_no_listeners_after_removing_the_only_one ()
		{
			signal.AddListener(listener.Method1);
			signal.RemoveListener(listener.Method1);

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch(V_0);

			listener.DidNotReceive().Method1(V_0);
		}

		[Test]
		public void should_have_no_listeners_after_removing_the_last_one ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.RemoveListener(listener.Method1);
			signal.RemoveListener(listener.Method2);

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch(V_0);

			listener.DidNotReceive().Method1(V_0);
			listener.DidNotReceive().Method2(V_0);
		}

		[Test]
		public void should_add_and_remove_listener_with_manage ()
		{
			signal.ManageListener(listener.Method1, true);
			
			Assert.That(signal.HasListeners(), Is.True);

			signal.ManageListener(listener.Method1, false);
			
			Assert.That(signal.HasListeners(), Is.False);
		}

		[Test]
		public void should_have_no_listeners_after_removing_all ()
		{
			signal.AddListener(listener.Method1);
			signal.AddListener(listener.Method2);
			signal.RemoveAllListeners();

			Assert.That(signal.HasListeners(), Is.False);

			signal.Dispatch(V_0);

			listener.DidNotReceive().Method1(V_0);
			listener.DidNotReceive().Method2(V_0);
		}

		public interface IListener
		{
			void Method1 (int t0);
			void Method2 (int t0);
		}
	}
}
