using MinLibs.MVC;

public class ApplyExampleRegistrations
{
    [Inject] public IContext context;

    public void Execute (
		MinLibs.Example.IExampleExternal minLibsExampleIExampleExternal,
		MinLibs.Example.IExampleExternal2 minLibsExampleIExampleExternal2
    )
    {
// >>> DECLARATIONS
		var minLibsExampleExampleCommand = new MinLibs.Example.ExampleCommand();
		var minLibsMVCContextSignals = new MinLibs.MVC.ContextSignals();
		var minLibsMVCMediators = new MinLibs.MVC.Mediators();
// <<< DECLARATIONS
            
            
// >>> REGISTRATIONS
		context.RegisterInstance(minLibsExampleIExampleExternal, RegisterFlags.None);
		context.RegisterInstance(minLibsExampleIExampleExternal2, RegisterFlags.None);
		context.RegisterInstance(minLibsExampleExampleCommand, RegisterFlags.PreventInjections);
		context.RegisterInstance<MinLibs.Signals.ISignalsManager>(minLibsMVCContextSignals, RegisterFlags.PreventInjections);
		context.RegisterInstance<MinLibs.MVC.IMediators>(minLibsMVCMediators, RegisterFlags.PreventInjections);
// <<< REGISTRATIONS
            
            
// >>> HANDLERS
		context.RegisterHandler<MinLibs.Example.IExampleModel>(host => {
			var minLibsExampleExampleModel = new MinLibs.Example.ExampleModel();
			context.OnCleanUp.AddListener(minLibsExampleExampleModel.Cleanup);
			minLibsExampleExampleModel.Init();
			return minLibsExampleExampleModel;
		}, RegisterFlags.NoCache | RegisterFlags.PreventInjections);

		context.RegisterHandler<MinLibs.MVC.RootMediator>(host => {
			var minLibsMVCRootMediator = new MinLibs.MVC.RootMediator();
			minLibsMVCRootMediator.mediators = context.Get<MinLibs.MVC.IMediators>();

			return minLibsMVCRootMediator;
		}, RegisterFlags.NoCache | RegisterFlags.PreventInjections);

		context.RegisterHandler<MinLibs.Example.ExampleMediator>(host => {
			var minLibsExampleExampleMediator = new MinLibs.Example.ExampleMediator();
			minLibsExampleExampleMediator.model = context.Get<MinLibs.Example.IExampleModel>();
			minLibsExampleExampleMediator.command = minLibsExampleExampleCommand;

			return minLibsExampleExampleMediator;
		}, RegisterFlags.NoCache | RegisterFlags.PreventInjections);

// <<< HANDLERS
            
            
// >>> ASSIGNMENTS
		minLibsExampleExampleCommand.model = context.Get<MinLibs.Example.IExampleModel>();

		minLibsMVCMediators.context = context.Get<MinLibs.MVC.IContext>();

// <<< ASSIGNMENTS
            
            
// >>> CLEANUPS
		context.OnCleanUp.AddListener(minLibsMVCContextSignals.Cleanup);
// <<< CLEANUPS
            
            
// >>> POSTINJECTIONS

// <<< POSTINJECTIONS
    }
}
