﻿using MinLibs.MVC;
using MinLibs.Signals;

namespace MinLibs.Example
{
	public class ExampleRegistrations : BaseRegistrations
	{
		public override RegistrationInfo Create()
		{
			var info = CreateInfo("Example");

			info.Add<IExampleExternal>().With(RegisterFlags.Parameter);
			info.Add<IExampleExternal2>().With(RegisterFlags.Parameter);
			info.Add<ExampleCommand>();
			info.Add<ContextSignals>().As<ISignalsManager>();
			info.Add<Mediators>().As<IMediators>();
			info.Add<ExampleModel>().As<IExampleModel>().With(RegisterFlags.NoCache).On(EnvFlags.CI);
			info.Add<ExampleModel2>().As<IExampleModel>().With(RegisterFlags.NoCache).On(EnvFlags.Staging | EnvFlags.Production);
			info.Add<RootMediator>().With(RegisterFlags.NoCache);
			info.Add<ExampleMediator>().With(RegisterFlags.NoCache);

			return info;
		}
	}
}
