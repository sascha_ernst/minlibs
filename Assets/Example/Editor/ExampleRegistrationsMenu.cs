﻿using MinLibs.MVC;
using UnityEditor;

namespace MinLibs.Example
{
	public static class ExampleRegistrationsMenu
	{
		const string envStatePath = "envStates/envState_google";

		[MenuItem("MinLibs/Registrations/Generate for Editor", false, 1)]
		public static void GenerateModularDryRun()
		{
			var envState = new EnvironmentState
			{
				editor = EditorFlags.IsEditor
			};

			ModularRegistrationsGenerator.GenerateRegistrations<ExampleRegistrations>(envState);
		}
	}
}
