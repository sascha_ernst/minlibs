﻿using MinLibs.MVC;

namespace MinLibs.Example
{
	public class ExampleExternal : IExampleExternal
	{
		[Inject] public ExampleCommand command;
	}

	public class ExampleExternal2 : IExampleExternal2
	{
		[Inject] public ExampleCommand command;
	}

	public interface IExampleExternal
	{
	}

	public interface IExampleExternal2
	{
	}
}
