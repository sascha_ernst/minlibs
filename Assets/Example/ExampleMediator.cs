using MinLibs.MVC;

namespace MinLibs.Example
{
	public class ExampleMediator : BehaviourMediator<IExampleView>
	{
		[Inject] public IExampleModel model;
		[Inject] public ExampleCommand command;
	}
}