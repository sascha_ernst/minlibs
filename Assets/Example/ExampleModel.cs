using MinLibs.MVC;

namespace MinLibs.Example
{
	public class ExampleModel : IExampleModel
	{
		[PostInjection]
		public void Init()
		{

		}

		[Cleanup]
		public void Cleanup()
		{

		}
	}

	public class ExampleModel2 : IExampleModel
	{

	}

	public interface IExampleModel
	{
	}
}