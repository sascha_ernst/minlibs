﻿using MinLibs.Example;
using MinLibs.MVC;
using UnityEngine;
using MinLibs.Signals;

public class ExampleView : MediatingBehaviour, IExampleView
{
	private IContext context;

	private void Start()
	{
		var startTime = Time.realtimeSinceStartup;

		context = new Context();

		var example1 = new ExampleExternal();
		var example2 = new ExampleExternal2();

		var isGenerated = Random.value > 0.5f;

		if (isGenerated)
		{
			context.Create<ApplyExampleRegistrations>().Execute(example1, example2);
		}
		else
		{
			context.RegisterInstance<IExampleExternal>(example1);
			context.RegisterInstance<IExampleExternal2>(example2);

			context.RegisterClass<ExampleCommand>();
			context.RegisterClass<ISignalsManager, ContextSignals>();
			context.RegisterClass<IMediators, Mediators>();
			context.RegisterClass<IExampleModel, ExampleModel>(RegisterFlags.NoCache);
			context.RegisterClass<RootMediator>(RegisterFlags.NoCache);
			context.RegisterClass<ExampleMediator>(RegisterFlags.NoCache);
		}

		var mediators = context.Get<IMediators>();
		mediators.Map<IExampleView, RootMediator>();
		mediators.Map<IExampleView, ExampleMediator>();

		mediators.Mediate(this);

		var endTime = Time.realtimeSinceStartup;

		var duration = endTime - startTime;
		Debug.LogWarning("duration " + duration + " when generated: " + isGenerated);
	}

	public override void HandleMediation()
	{
		base.HandleMediation();

		Debug.Log("HandleMediation");
	}
}

public interface IExampleView : IMediatingBehaviour
{
}