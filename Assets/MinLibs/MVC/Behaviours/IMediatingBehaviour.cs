namespace MinLibs.MVC
{
	public interface IMediatingBehaviour : IMediatedBehaviour, IMediating
	{
		
	}
}
