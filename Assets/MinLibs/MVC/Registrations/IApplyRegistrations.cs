namespace MinLibs.MVC
{
	public interface IApplyRegistrations
	{
		void Execute(params object[] args);
	}
}