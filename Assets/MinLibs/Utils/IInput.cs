namespace MinLibs.Utils
{
	public interface IInput
	{
		bool IsDown(string key);
	}
}